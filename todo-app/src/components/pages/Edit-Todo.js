import React, { Component } from "react";
import axios from "axios";
import { Link } from "react-router-dom";
export default class EditTodo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      description: "",
      completed: false
    };
    this.onChangeDescription = this.onChangeDescription.bind(this);
    this.onChangeCompleted = this.onChangeCompleted.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  async componentDidMount() {
    try {
      const token = localStorage.getItem("jwtToken");
      const res = await axios.get(
        "http://localhost:3001/todos/" + this.props.match.params.id,
        {
          headers: { Authorization: `Bearer ${token}` }
        }
      );
      this.setState({
        description: res.data.description,
        completed: res.data.completed
      });
    } catch (e) {
      alert(e);
    }
  }
  onChangeDescription(e) {
    this.setState({
      description: e.target.value
    });
  }

  onChangeCompleted() {
    this.setState({
      completed: !this.state.completed
    });
  }

  async onSubmit(e) {
    try {
      e.preventDefault();
      const token = localStorage.getItem("jwtToken");
      const { description, completed } = this.state;
      const obj = {
        description,
        completed
      };
      const res = await axios.patch(
        "http://localhost:3001/todos/" + this.props.match.params.id,
        obj,
        {
          headers: { Authorization: `Bearer ${token}` }
        }
      );
      this.setState({
        description: res.data.description,
        completed: res.data.completed
      });
      if (res.status === 200) {
        alert("Successfully Update the data");
        this.props.history.push("/home");
      }
    } catch (e) {
      alert(e);
    }
  }

  render() {
    return (
      <div className="container">
        <h3 align="center">Update Todo</h3>
        <form onSubmit={this.onSubmit}>
          <div className="form-group">
            <label>Description: </label>
            <textarea
              className="form-control"
              rows="5"
              value={this.state.description}
              onChange={this.onChangeDescription}
            ></textarea>
          </div>
          <div className="form-check">
            <input
              className="form-check-input"
              id="completedCheckbox"
              type="checkbox"
              name="completedCheckbox"
              onChange={this.onChangeCompleted}
              checked={this.state.completed}
              value={this.state.completed}
            />
            <label className="form-check-label">Completed</label>
          </div>

          <br />

          <div className="form-group">
            <input
              type="submit"
              value="Update Todo"
              className="btn btn-primary"
            />
          </div>
          <br />
        </form>
        <div className="form-group">
          <Link to="/home">
            <input
              type="submit"
              value="Back"
              float="right"
              className="btn btn-primary"
            />
          </Link>
        </div>
      </div>
    );
  }
}