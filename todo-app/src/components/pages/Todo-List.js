import React, { Component } from "react";
import { Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import AppHeader from "../common/AppHeader";
import Pagination from "react-js-pagination";

import axios from "axios";
//require("bootstrap/less/bootstrap.less");


const Todo = props => (
  <tr>
    <td>{props.todo.description}</td>
    <td>{props.todo.completed}</td>
    <td>
      <Link to={"/edit/" + props.todo._id}>Update</Link>
    </td>
    <td>
      <Link to={"/delete/" + props.todo._id}>Delete</Link>
    </td>
  </tr>
);

class TodosList extends Component {
  constructor(props) {
    super(props);
    this.state = { todos: [] };
    this.onLogout = this.onLogout.bind(this);
  }

  onLogout(e) {
    e.preventDefault();
    localStorage.removeItem("jwtToken");
    this.props.history.push("/");
  }
  

  async componentDidMount() {
    const token = localStorage.getItem("jwtToken");
    try {
      const res = await axios.get("http://localhost:3001/todos?completed=false", {
        headers: { Authorization: `Bearer ${token}` }
      });
      console.log(res.data);
      this.setState({ todos: res.data });
    } catch {
      alert("Error occur");
    }
  }
  todoList() {
    return this.state.todos.map(function(currentTodo, i) {
      return <Todo todo={currentTodo} key={i} />;
    });
  }
  render() {
    return (
      <div className="container">
        <AppHeader />
        <h3>Todos List</h3>
        <form>
          <div className="form-group">
            <input
              type="submit"
              value="Logout"
              className="btn btn-primary"
              onClick={this.onLogout}
            />
          </div>
        </form>
        <table className="table table-striped" style={{ marginTop: 20 }}>
          <thead>
            <tr>
              <th>Description</th>
            </tr>
          </thead>
          <tbody>{this.todoList()}</tbody>
        </table>
        
      </div>
    );
  }
}
export default TodosList;
