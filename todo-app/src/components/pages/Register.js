import React, { Component } from "react";
import { Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import axios from "axios";

export default class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Username: "",
      email: "",
      password: ""
    };
    this.onChangeUsername = this.onChangeUsername.bind(this);
    this.onChangeEmail = this.onChangeEmail.bind(this);
    this.onChangePassword = this.onChangePassword.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onChangeUsername(e) {
    this.setState({
      Username: e.target.value
    });
  }

  onChangeEmail(e) {
    this.setState({
      email: e.target.value
    });
  }

  onChangePassword(e) {
    this.setState({
      password: e.target.value
    });
  }

  async onSubmit(e) {
    try {
      e.preventDefault();
      const { Username, email, password } = this.state;
      const newUser = {
        Username,
        email,
        password
      };

      const res = await axios.post("http://localhost:3001/users", newUser);
      if (res.status === 201) {
        alert("Successfully Upload data");
        this.props.history.push("/");
      }
      this.setState({
        Username: "",
        email: "",
        password: ""
      });
    } catch {
      return "Error";
    }
  }

  render() {
    const { Username, email, password } = this.state;
    return (
      <div className="container" style={{ marginTop: 10 }}>
        <h3>Register New User</h3>
        <form onSubmit={this.onSubmit}>
          <div className="form-group">
            <label>User Name: </label>
            <input
              type="text"
              className="form-control"
              onChange={this.onChangeUsername}
              value={Username}
              required
            />
          </div>
          <div className="form-group">
            <label>Email: </label>
            <input
              type="email"
              className="form-control"
              onChange={this.onChangeEmail}
              value={email}
              required
            />
          </div>
          <div className="form-group">
            <label>Password: </label>
            <input
              type="password"
              className="form-control"
              onChange={this.onChangePassword}
              value={password}
              required
            />
          </div>
          <div className="form-group">
            <input type="submit" value="Register" className="btn btn-primary" />
          </div>
        </form>
        <br />
        <div className="form-group">
          <Link to="/">
            <input type="submit" value="Back" className="btn btn-primary" />
          </Link>
        </div>
      </div>
    );
  }
}